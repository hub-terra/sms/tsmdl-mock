## major.minor.patch (unreleased)
### Changed
### Added
### Fixed

## 1.0.1 - 2024-07-16
### Added
- extra attribute "sta_endpoint" to datasource

## 1.0.0 - 2024-06-19
- Inital Release