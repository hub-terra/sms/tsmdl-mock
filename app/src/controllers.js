const data = require('../data/data.json');

const getDatasource = (req, res) => {
    const datasource_id = extractId(req.params.datasource_id);
    const datasource = data.datasources.find(datasource => datasource['@iot.id'] === datasource_id);
    
    if(datasource){
        res.json(mapAttributes(datasource));
    }else{
        send404(res)
    }
    
};

const getDatasources = (req, res) => {
    res.json({value: data.datasources});
};

const getThing = (req, res) => {
    const datasource_id = extractId(req.params.datasource_id);
    const thing_id = extractId(req.params.thing_id);
    const thing = data.things.find(thing => thing.datasource_id === datasource_id && thing['@iot.id'] === thing_id);
    
    if(thing){
        res.json(mapAttributes(thing));
    }else{
        send404(res);
    }
    
};

const getThings = (req, res) => {
    const datasource_id = extractId(req.params.datasource_id);
    const things = data.things.filter(thing => thing.datasource_id === datasource_id).map(mapAttributes);
    res.json({value: things});
};

const getDatastream = (req, res) => {
    const datasource_id = extractId(req.params.datasource_id);
    const thing_id = extractId(req.params.thing_id);
    const datastream_id = extractId(req.params.datastream_id);
    const datastream = data.datastreams.find(datastream => datastream.datasource_id === datasource_id && datastream.thing_id === thing_id && datastream['@iot.id'] === datastream_id);
    
    if(datastream){
        res.json(mapAttributes(datastream));
    }else {
        send404(res);
    }
    
};
const getDatastreams = (req, res) => {
    const datasource_id = extractId(req.params.datasource_id);
    const thing_id = extractId(req.params.thing_id);
    const datastreams = data.datastreams.filter(datastream => datastream.datasource_id === datasource_id && datastream.thing_id === thing_id).map(mapAttributes);;
    res.json({value: datastreams});
};

const getObservations = (req, res) => {
    const datasource_id = extractId(req.params.datasource_id);
    const thing_id = extractId(req.params.thing_id);
    const datastream_id = extractId(req.params.datastream_id);
    const observations = data.observations.filter(observation => observation.datasource_id === datasource_id && observation.thing_id === thing_id && observation.datastream_id === datastream_id);
    res.json({value: observations});
};

const extractId = (param) => {
    return param.replace(/[()]/g, '');
};

const send404 = (res) => {
    res.status(404).send("Sorry can't find that!");
}

const mapAttributes = (entity) => {
    
    let defaultMap = {
        "@iot.id": entity["@iot.id"],
        "name":entity.name,
        "description": entity.description,
        "properties": entity.properties
    } 
    
    if(entity["sta_endpoint"]){
        defaultMap["sta_endpoint"] = entity["sta_endpoint"];
    }
    
    return defaultMap
}

module.exports = {
    getDatasource,
    getThing,
    getDatastream,
    getDatasources,
    getThings,
    getDatastreams,
    getObservations
};

