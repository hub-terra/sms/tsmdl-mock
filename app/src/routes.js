const express = require('express');
const router = express.Router();
const {
    getDatasources,
    getThings,
    getDatastreams,
    getObservations,
    getDatasource,
    getThing,
    getDatastream
} = require('./controllers');

router.get('/Datasources', getDatasources);
router.get('/Datasources(:datasource_id)', getDatasource);
router.get('/Datasources(:datasource_id)/Things', getThings);
router.get('/Datasources(:datasource_id)/Things(:thing_id)', getThing);
router.get('/Datasources(:datasource_id)/Things(:thing_id)/Datastreams', getDatastreams);
router.get('/Datasources(:datasource_id)/Things(:thing_id)/Datastreams(:datastream_id)', getDatastream);
router.get('/Datasources(:datasource_id)/Things(:thing_id)/Datastreams(:datastream_id)/Observations', getObservations);

module.exports = router;

