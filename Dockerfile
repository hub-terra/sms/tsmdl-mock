FROM node:19-slim
RUN mkdir -p /home/node/app && chown -R node:node /home/node/app

# Set working directory
WORKDIR /home/node/app

# Copy package.json and package-lock.json for dependency installation
COPY --chown=node:node ./app/package*.json ./

USER node

# Install dependencies
RUN npm install

# Copy source code
COPY --chown=node:node ./app .

CMD ["node", "src/index.js"]