# Mock Servcie for TSMDL (TimeSeriesManagement Decoupling Layyer)
[![Latest Release](https://codebase.helmholtz.cloud/hub-terra/sms/tsmdl-mock/-/badges/release.svg)](https://codebase.helmholtz.cloud/hub-terra/sms/tsmdl-mock/-/releases)

- uses express.js [link](https://expressjs.com/)
- This service is just a mock for the TimeSeriesManagement Decoupling Layyer (tsmdl).
- It's just used for local development of the Sensor Management System [Repo](https://codebase.helmholtz.cloud/hub-terra/sms/orchestration) 
## Setup
- git clone
- `cd tsmdl-dummy-server`
- `docker compose up -d`
- server starts listening on port 3000

## Routes
- routes accoding to [tsmd-api specification](https://codebase.helmholtz.cloud/hub-terra/tsmdl-time-series-management-decoupling-layer/tsmdl-api/-/blob/main/openapi.json)

## Environment variables
- `PORT`
  - if set, you can change the port on which the application runs